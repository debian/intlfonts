#!/bin/sh

# Invoke this script at the top of source code tree.

DIRS="Asian Chinese Chinese.BIG Ethiopic European European.BIG"
DIRS="$DIRS Japanese Japanese.BIG Misc"

for BDF in $(find $DIRS -name '*.bdf'); do
  PCF=${BDF%bdf}pcf
  bdftopcf -o $PCF $BDF
  gzip -9n $PCF
done
